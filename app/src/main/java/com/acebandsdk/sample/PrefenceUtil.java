package com.acebandsdk.sample;

import android.content.Context;
import android.content.SharedPreferences;

import com.xiaoqu.aceband.ble.util.AppContextUtil;

/**
 * Created by chenshen on 16/3/30.
 */
public class PrefenceUtil {

    private static final String PRE_NAME = "setting_preferance";
    public static final String PRE_HAS_BIND_DEVICE = "has_bind_device";


    private static SharedPreferences getSharedPreferences() {
        return AppContextUtil.getInstance()
                .getSharedPreferences(PRE_NAME, Context.MODE_PRIVATE);
    }


    public static boolean readBooleanPreference(String key,boolean defaultValue) {
        SharedPreferences pref =getSharedPreferences();
        return pref.getBoolean(key, defaultValue);
    }


    public static void writePreference(String key, Object value) {
        SharedPreferences pref = getSharedPreferences();
        if(value instanceof  Boolean){
            pref.edit().putBoolean(key, (Boolean)value).commit();
        }else if(value instanceof  String){
            pref.edit().putString(key, (String) value).commit();
        }else if(value instanceof Integer){
            pref.edit().putInt(key,(Integer)value).commit();
        }else if(value instanceof Long){
            pref.edit().putLong(key,(Long)value).commit();
        }else if(value instanceof Float){
            pref.edit().putFloat(key,(Float)value).commit();
        }
        pref.edit().apply();

    }



    public static Object readPreference(String key,Object defalutValue) {
        Object result = null;
        SharedPreferences pref = getSharedPreferences();
        if(defalutValue instanceof  Boolean){
            result = pref.getBoolean(key,(Boolean) defalutValue);
        }else if(defalutValue instanceof  String){
            result = pref.getString(key,(String) defalutValue);
        }else if(defalutValue instanceof Integer){
            result = pref.getInt(key,(Integer) defalutValue);
        }else if(defalutValue instanceof Long){
            result = pref.getLong(key,(Long) defalutValue);
        }else if(defalutValue instanceof Float){
            result = pref.getFloat(key,(Float) defalutValue);
        }
        return result;


    }


}
