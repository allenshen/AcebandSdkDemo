package com.acebandsdk.sample;


import android.app.Activity;
import android.app.ProgressDialog;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.kyleduo.switchbutton.SwitchButton;
import com.xiaoqu.aceband.sdk.AceBandSDKManager;
import com.xiaoqu.aceband.sdk.AuthorizeCallback;
import com.xiaoqu.aceband.sdk.BindOrUnBindDeviceCallback;
import com.xiaoqu.aceband.sdk.BleSyncProgressListener;
import com.xiaoqu.aceband.sdk.BleTaskListener;
import com.xiaoqu.aceband.sdk.DeviceStateListener;
import com.xiaoqu.aceband.sdk.MessageRemindType;
import com.xiaoqu.aceband.sdk.OtaCallback;


import java.util.Calendar;

import butterknife.Bind;
import butterknife.ButterKnife;

public class MainActivity extends Activity implements OnClickListener {
    @Bind(R.id.back)
    ImageView back;
    @Bind(R.id.battery_bg_view)
    ImageView batteryBgView;
    @Bind(R.id.battery_value_view)
    ImageView batteryValueView;
    @Bind(R.id.battery_layout)
    FrameLayout batteryLayout;
    @Bind(R.id.connect_tv)
    TextView connectTv;
    @Bind(R.id.showmsg_tv)
    TextView showMsgTv;

    @Bind(R.id.pushinfo)
    TextView pushinfoTv;


    @Bind(R.id.hearate_test_switch)
    SwitchButton hearateTestSwitch;
    @Bind(R.id.sitting_remind)
    TextView sittingRemind;
    @Bind(R.id.sitting_remind_switch)
    SwitchButton sittingRemindSwitch;
    @Bind(R.id.disconnect_switch)
    SwitchButton disconnectSwitch;
    @Bind(R.id.take_photo)
    Button takePhoto;
    @Bind(R.id.update)
    Button update;
    @Bind(R.id.set_alarm_time)
    Button setAlarmTime;
    @Bind(R.id.call_remind)
    Button callRemind;
    @Bind(R.id.sms_remind)
    Button smsRemind;
    @Bind(R.id.qq_remind)
    Button qqRemind;
    @Bind(R.id.wechat_remind)
    Button wechatRemind;
    @Bind(R.id.control_layout)
    ScrollView controlLayout;
    @Bind(R.id.upHandLightSwitch)
    SwitchButton upHandLightSwitch;
    @Bind(R.id.auto_hearate_test)
    SwitchButton autoHeartrateTestSwitch;
    @Bind(R.id.set_show_time_format)
    SwitchButton setShowTimeFormat;
    private Context mContext;
    private float totalPowerWith;
    private AceBandSDKManager sdkManager;
    public final int FILE_SELECT_CODE = 2000;
    private DeviceStateListener connectStateListener = new DeviceStateListener() {
        @Override
        public void connect(String deviceName) {
            connectTv.setText(deviceName+ "已连接");
            controlLayout.setVisibility(View.VISIBLE);

        }

        @Override
        public void disConnect(String deviceName) {
            connectTv.setText(deviceName + "连接已断开");
        }

        @Override
        public void rssiChange(int i) {

        }

        @Override
        public void pushBatteryPower(int powerpersent) {
            setPowerValue(powerpersent);
        }

        @Override
        public void pushHeartRateData(String json) {
            pushinfoTv.setText(json);
        }

        @Override
        public void pushSportData(String json) {
            pushinfoTv.setText(json);
        }

        @Override
        public void takePhoto() {
            pushinfoTv.setText("设备发起拍照指令");
        }

        @Override
        public void deviceFindPhone() {
            pushinfoTv.setText("设备发起查找手机指令");
        }
    };


    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        mContext = this;
        addListener();
        totalPowerWith = batteryValueView.getLayoutParams().width;
        setPowerValue(0);
        sdkManager = AceBandSDKManager.getInstance();
        sdkManager.registerDeviceStateListener(connectStateListener);
        if(PrefenceUtil.readBooleanPreference(PrefenceUtil.PRE_HAS_BIND_DEVICE,false)){
            sdkManager.connectBindDevice();
        }else{
            final BluetoothDevice device = getIntent().getParcelableExtra("BluetoothDevice");
            progressDialog = new ProgressDialog(this);
            progressDialog.setTitle("正在验证appid，请稍后");
            progressDialog.setCanceledOnTouchOutside(false);
            progressDialog.show();
            sdkManager.authorize(Constances.APPID, Constances.APPKEY,Constances.PRIVATEKEY, new AuthorizeCallback() {
                @Override
                public void onComplete(String scode, String expire_time) {

                    progressDialog.dismiss();
                    sdkManager.bindDevice(device.getAddress(), device.getName());
                    PrefenceUtil.writePreference(PrefenceUtil.PRE_HAS_BIND_DEVICE,true);
                    Toast.makeText(mContext,"验证appid成功",Toast.LENGTH_SHORT).show();

                }

                @Override
                public void onError(Exception e) {
                    progressDialog.dismiss();
                    Toast.makeText(mContext,"验证appid出错",Toast.LENGTH_SHORT).show();
                }
            });
        }
    }






    private void addListener(){
        back.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        hearateTestSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean check) {

                if (check) {
                   sdkManager.openHeartrateTest(new BleTaskListener() {
                       @Override
                       public void onSuccess(String jsonData) {
                           pushinfoTv.setText(jsonData);
                       }

                       @Override
                       public void onFailed(String jsonData) {
                           pushinfoTv.setText(jsonData);
                       }
                   },60*3);
                }else{
                    sdkManager.closeHeartrateTest(new BleTaskListener() {
                        @Override
                        public void onSuccess(String jsonData) {
                            pushinfoTv.setText(jsonData);
                        }

                        @Override
                        public void onFailed(String jsonData) {
                            pushinfoTv.setText(jsonData);
                        }
                    });
                }

            }
        });
        sittingRemindSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                int sittingtime = 0;
                if(b){
                    sittingtime = 5;
                }
                sdkManager.setLongSittigTime(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },sittingtime,8,30,21,30);

            }
        });
        disconnectSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                sdkManager.swithDisConnectRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },isChecked);
            }
        });

        upHandLightSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                sdkManager.openOrCloseHandLight(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },isChecked);
            }
        });
        autoHeartrateTestSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isOpen) {
                int startHour = 23;
                int statMin = 0;
                int endHour = 23;
                int endMin = 5;
                sdkManager.autoTestHeartRate(new BleTaskListener() {
                    @Override
                    public void onSuccess(String s) {

                    }

                    @Override
                    public void onFailed(String s) {

                    }
                },isOpen,startHour,statMin,endHour,endMin);
            }
        });

        setShowTimeFormat.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isAmPmFormat) {
                sdkManager.setShowTimeFormat(new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                },isAmPmFormat);
            }
        });
    }


    @Override
    protected void onDestroy() {
        // TODO Auto-generated method stubsdkManager.unregisterDeviceStateListener(connectStateListener);
        sdkManager.onDestory();
        super.onDestroy();


    }

//    public void onEvent(BandPushData bandPushData) {
//        DebugUtil.LogI("OnEvent", bandPushData.toString());
//        setPowerValue(bandPushData.power);
//        stepCircleView.setProgress(bandPushData.steps);
//        if (bandPushData.bandHearate != null)
//            hearateCircleView.setHearateRate(bandPushData.bandHearate.heartRateCount);
//        showMsgTv.setText("手环固件版本:"+bandPushData.version);
//        pushinfoTv.setText(bandPushData.toString());
//    }

    private void setPowerValue(int powerValue) {

        float batteryValueWidth = powerValue / 100.0f * totalPowerWith;
        FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) batteryValueView.getLayoutParams();
        params.width = (int) batteryValueWidth;
        batteryValueView.setLayoutParams(params);
    }



    @Override
    public void onClick(View view) {
        String title;
        String content;
        switch (view.getId()) {

            case R.id.unbind:
                sdkManager.unBindDevice(new BindOrUnBindDeviceCallback() {
                    @Override
                    public void onComplete(String s) {
                        PrefenceUtil.writePreference(PrefenceUtil.PRE_HAS_BIND_DEVICE,false);
                        Intent intent = new Intent(MainActivity.this,BleDeviceListActivity.class);
                        startActivity(intent);
                        finish();
                    }

                    @Override
                    public void onError(String s) {
                        Toast.makeText(mContext,s,Toast.LENGTH_SHORT).show();
                    }
                });
                break;

            case R.id.reboot:
                sdkManager.rebootDevice();
                break;

            case R.id.set_userinfo:
                int sex =0;
                int age = 25;
                float height = 170;
                float weight = 65;
                String uid = "10000";
                int unit = 0;//0表示设置手环显示距离单位为公制 ,1表示设置为英制
                sdkManager.syncUserMsgToDevice(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },sex,age,height,weight,unit,uid);
                break;

            case R.id.disconnect:

                sdkManager.disConnectDevice();

                break;

            case R.id.reconnect:
                connectTv.setText("正在连接");
                sdkManager.connectBindDevice();

                break;

            case R.id.get_new_sport_msg:
                sdkManager.getNewSportMsg(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.sync_all_data:

                sdkManager.syncAllBandData(new BleSyncProgressListener() {
                    @Override
                    public void progress(int i) {
                        pushinfoTv.setText("进度:"+String.valueOf(i)+"%");
                    }

                    @Override
                    public void syctodayDataOver() {
                        pushinfoTv.setText("当天数据同步完成可以调用查询接口先显示当天的睡眠运动等数据");
                    }

                    @Override
                    public void onSuccess(String jsonData) { //同步完成后需调用查询数据接口获取你需要的睡眠运动,心率等数据
                        pushinfoTv.setText("同步完成:"+jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.sync_sport:
                sdkManager.syncSportData(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;


            case R.id.sync_heartrate:
                sdkManager.syncHeartRateData(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;



            case R.id.take_photo:
                sdkManager.enterPhotoMode(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },true);

                break;

            case R.id.set_alarm_time:
                Calendar calendar = Calendar.getInstance();
                int hour = calendar.get(Calendar.HOUR_OF_DAY);
                int minute = calendar.get(Calendar.MINUTE)+2;
                int[] repeatDay = new int[]{0,0,0,0,0,0,0};
                String alarmLabel = "起床";
                int alarmIndex = 0;
                int enable = 1;
                final boolean isRepeat = false;
                sdkManager.setAlarm(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },alarmIndex,enable,hour,minute,repeatDay,isRepeat);

                break;



            case R.id.update:

                Intent mIntent = new Intent(getApplicationContext(),
                        FileSelectActivity.class);
                startActivityForResult(mIntent,FILE_SELECT_CODE);



                break;

            case R.id.get_band_info:
//                byte[] cmdArray = new byte[]{0x0E,0x12,0x34,0x56,0x78,(byte)0x9A,(byte)0xBC,(byte)0xDE,(byte)0xF0,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00,0x00};
//                BleJustSendCmdTask task = new BleJustSendCmdTask(cmdArray);
//                task.work();
                sdkManager.getDeviceInfo(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.find_band:
                sdkManager.findDevice(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },1);
                break;

            case R.id.query_power:
                sdkManager.getBatteryPower(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });

//                String stepsJson = gson.toJson(uploadStepBeanList).replace("[","").replace("]","");
//                String sleepjson = gson.toJson(uploadSleepBeenList).replace("[","").replace("]","");
//                String heratejson = gson.toJson(uploadHeartBeanList).replace("[","").replace("]","");
//                DebugUtil.logBle("stepsjson:"+stepsJson);
//                DebugUtil.logBle("sleepjson:"+sleepjson);
//                DebugUtil.logBle("heratejson:"+heratejson);

                break;

            case R.id.set_sleep_time:
                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 12);
                calendar.set(Calendar.MINUTE, 12);
                long noonstarttime = calendar.getTimeInMillis();
                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 14);
                calendar.set(Calendar.MINUTE, 0);
                long noonendTime  = calendar.getTimeInMillis();

                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 22);
                calendar.set(Calendar.MINUTE, 31);
                long nightstartTime = calendar.getTimeInMillis();
                calendar = Calendar.getInstance();
                calendar.set(Calendar.HOUR_OF_DAY, 7);
                calendar.set(Calendar.MINUTE, 30);
                long nightendtime  = calendar.getTimeInMillis();
                sdkManager.setSleepTime(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },noonstarttime,noonendTime,nightstartTime,nightendtime);
                break;



            case R.id.call_remind:

                title = "123456789123456789";
                content = "123456789123456789";
                Log.i("callremind",title);
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.PHONE_TASNS_TYPE,title,content);
                break;

            case R.id.sms_remind:
                title = "张学友";
                content = "你在哪里呀";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.SMS_TASNS_TYPE,title,content);
                break;

            case R.id.qq_remind:
                title = "郭富城";
                content = "今晚开演唱会";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.QQ_TASNS_TYPE,title,content);
                break;

            case R.id.wechat_remind:
                title = "黎明";
                content = "今夜你会不会来";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.WECHAT_TASNS_TYPE,title,content);
                break;

            case R.id.fbmessage_remind:
                title = "黎明";
                content = "facbook message";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.FACEBOOK_MESSAGE_TASNS_TYPE,title,content);
                break;


            case R.id.twitter_remind:
                title = "周杰伦";
                content = "无与伦比演唱会开始";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.TWITTER_TASNS_TYPE,title,content);
                break;

            case R.id.skype_remind:
                title = "江疏影";
                content = "你的未来是我吗?";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.SKYPE_TASNS_TYPE,title,content);
                break;

            case R.id.whatsapp_remind:
                title = "胡歌";
                content = "仙剑奇侠传6出来了";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.WHATSAPP_TASNS_TYPE,title,content);

                break;

            case R.id.facebook_remind:
                title = "谢霆锋";
                content = "因为爱所以爱中国";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.FACEBOOK_TASNS_TYPE,title,content);
                break;

            case R.id.line_remind:
                title = "line remind";
                content = "line message";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.LINE_TASNS_TYPE,title,content);
                break;

            case R.id.other_app_remind:
                title = "other appremind";
                content = "其他应用信息提醒";
                sdkManager.sendTextRemind(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                }, MessageRemindType.FACEBOOK_MESSAGE_TASNS_TYPE,title,content);
                break;


            case R.id.query_time_step:
                Calendar calendar1 = Calendar.getInstance();
                long endTime = calendar1.getTimeInMillis();
                calendar1.add(Calendar.DAY_OF_YEAR,-2);
                long startTime = calendar1.getTimeInMillis();
                sdkManager.getSportDataByTime(startTime, endTime, new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });

                break;

            case R.id.query_time_sleep:
                calendar1 = Calendar.getInstance();
                endTime = calendar1.getTimeInMillis();
                calendar1.add(Calendar.DAY_OF_YEAR,-2);
                startTime = calendar1.getTimeInMillis();
                sdkManager.getSleepDataByTime(startTime, endTime, new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.set_alarm_withname:
                calendar = Calendar.getInstance();
                 hour = calendar.get(Calendar.HOUR_OF_DAY);
                 minute = calendar.get(Calendar.MINUTE)+2;
                repeatDay = new int[]{1,1,1,1,1,1,1};
                String alarmName = "事件提醒";
                final boolean isRepeatDay = true;
                sdkManager.setAlarm(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                },1,2,hour,minute,repeatDay,alarmName,isRepeatDay);
                break;

            case R.id.query_device_charge_state:
                sdkManager.queryDeviceIsCharge(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.resetDevice:
                sdkManager.resetDevice(new BleTaskListener() {
                    @Override
                    public void onSuccess(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }

                    @Override
                    public void onFailed(String jsonData) {
                        pushinfoTv.setText(jsonData);
                    }
                });
                break;

            case R.id.query_heart_data:
                calendar = Calendar.getInstance();
                long dateTimeMills = calendar.getTimeInMillis();
                int startTimeSeconds = 0;
                int endTimeSeconds = 7*60*60;
                sdkManager.getHeartRateDataByTime(dateTimeMills, startTimeSeconds, endTimeSeconds, new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                });
                break;

            case R.id.set_sport_target:
                sdkManager.setTargetSteps(new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                },10000);
                break;

            case R.id.set_device_imei:

                sdkManager.setDeviceImei(new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                },"123456789ABCDEF");

                break;

            case R.id.get_device_imei:

                sdkManager.getDeviceImei(new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                });

                break;

            case R.id.get_day_sleepinfo:
                calendar = Calendar.getInstance();
                long dateTimeInMills = calendar.getTimeInMillis();
                sdkManager.getDaySleepInfo(dateTimeInMills,new BleTaskListener() {
                    @Override
                    public void onSuccess(String json) {
                        pushinfoTv.setText(json);
                        Log.v("daySleepInfo",json);
                    }

                    @Override
                    public void onFailed(String json) {
                        pushinfoTv.setText(json);
                    }
                });
                break;

        }

    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        switch(requestCode){
            case FILE_SELECT_CODE:
                if(resultCode == RESULT_OK)
                {
                    String filePath = null;
                    filePath = data.getStringExtra("selectfile");
                    if(filePath != null)
                    {
                        Log.v("filePath",filePath);
                        sdkManager.updateFirmare(new OtaCallback() {
                            @Override
                            public void onProgress(int progress) {
                                showMsgTv.setText("升级进度:"+progress);
                            }

                            @Override
                            public void onStart(String startObject) {
                                showMsgTv.setText("开始升级");
                            }

                            @Override
                            public void onFailed(String error) {
                                showMsgTv.setText("升级失败");
                            }

                            @Override
                            public void onSuccess() {
                                showMsgTv.setText("升级成功");
                            }

                            @Override
                            public void onPrepareUpdate(String message) {
                                showMsgTv.setText(message);
                            }

                            @Override
                            public void onPostUpdateRestart(String message) {
                                showMsgTv.setText(message);
                            }
                        },filePath);

                    }
                }
                break;
        }
        super.onActivityResult(requestCode,resultCode,data);
    }



}
