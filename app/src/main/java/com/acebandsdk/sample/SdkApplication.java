package com.acebandsdk.sample;

import android.app.Application;

import com.xiaoqu.aceband.sdk.AceBandSDKManager;

/**
 * Created by Administrator on 2016/7/15.
 */
public class SdkApplication extends Application {

    @Override
    public void onCreate(){
        super.onCreate();
       AceBandSDKManager.getInstance().init(this);

    }
}
