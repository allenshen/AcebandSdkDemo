/**  
* @Title: BleDeviceListAdapter.java
* @Package com.veclink.bledemo
* @Description: TODO(用一句话描述该文件做什么)
* @author Allen 
* @date 2014-9-6 上午11:26:44
* @version V1.0  
*/ 
package com.acebandsdk.sample;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.xiaoqu.aceband.ble.bean.BandDeviceBean;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;

/**
 * @author Allen
 *
 */
public class BleDeviceListAdapter  extends BaseAdapter{
	
	public List<BandDeviceBean> listItems = new ArrayList<BandDeviceBean>();
	public HashMap<String,String> macaddressMap = new HashMap<>();
	private Context mContext;
	public BleDeviceListAdapter(Context mContext){
		this.mContext = mContext;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getCount()
	 */
	@Override
	public int getCount() {
		// TODO Auto-generated method stub
		return listItems.size();
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItem(int)
	 */
	@Override
	public Object getItem(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getItemId(int)
	 */
	@Override
	public long getItemId(int arg0) {
		// TODO Auto-generated method stub
		return arg0;
	}

	/* (non-Javadoc)
	 * @see android.widget.Adapter#getView(int, android.view.View, android.view.ViewGroup)
	 */
	
	public final class ViewHolder{
		public TextView name_view;
		public TextView address_view;
		public TextView rssi_view;
		public TextView pass_or_not_view;
	}
	@Override
	public View getView(int position, View contentView, ViewGroup arg2) {
		ViewHolder holder;
		final BandDeviceBean device = listItems.get(position);
		if(contentView==null){
			holder = new ViewHolder();
			contentView = LayoutInflater.from(mContext).inflate(R.layout.list_item, null);
			holder.name_view = (TextView) contentView.findViewById(R.id.deviceName);
			holder.address_view = (TextView) contentView.findViewById(R.id.deviceAddr);
			holder.rssi_view = (TextView) contentView.findViewById(R.id.deviceRssi);
			holder.pass_or_not_view = (TextView)contentView.findViewById(R.id.pass_or_not);
			contentView.setTag(holder);
		}else{
			holder = (ViewHolder) contentView.getTag();
		}		
		holder.name_view.setText(device.getName());
		holder.address_view.setText(device.getAddress());
		holder.rssi_view.setText(String.valueOf(device.getRssi()));
//		holder.pass_or_not_view.setVisibility(View.GONE);
		if(device.getRssi()>=-55){
			holder.rssi_view.setTextColor(Color.GREEN);
			holder.pass_or_not_view.setTextColor(Color.GREEN);
			holder.pass_or_not_view.setText("通过");
		}else{
			holder.rssi_view.setTextColor(Color.RED);
			holder.pass_or_not_view.setTextColor(Color.RED);
			holder.pass_or_not_view.setText("失败");
		}
		return contentView;
	}

	public void addDeviceItem(BandDeviceBean device) {
		this.listItems.add(device);
		this.macaddressMap.put(device.getAddress(),device.getAddress());
		Collections.sort(this.listItems);
		this.notifyDataSetChanged();



	}
	public void addDeviceItems(List<BandDeviceBean> devices) {
		this.listItems.addAll(devices);
		for (BandDeviceBean bandDeviceBean:devices){
			this.macaddressMap.put(bandDeviceBean.getAddress(),bandDeviceBean.getAddress());
		}
		Collections.sort(this.listItems);
		this.notifyDataSetChanged();



	}
	
	public void clearAllDevieceItem(){
		this.listItems.clear();
		this.macaddressMap.clear();
		this.notifyDataSetChanged();
	}

}
