package com.acebandsdk.sample;

import android.Manifest;
import android.app.Activity;
import android.content.pm.PackageManager;
import android.os.Build;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by chenshen on 16/8/14.
 */
public class GetPermissionUtil {
    public static void getPermissions(final Activity activity){
        if (Build.VERSION.SDK_INT >= 23) {
            List<String> permissionsNeeded = new ArrayList<>();
            permissionsNeeded.add(Manifest.permission.READ_CONTACTS);
            permissionsNeeded.add(Manifest.permission.ACCESS_COARSE_LOCATION);
            permissionsNeeded.add(Manifest.permission.ACCESS_FINE_LOCATION);
            permissionsNeeded.add(Manifest.permission.RECORD_AUDIO);
            permissionsNeeded.add(Manifest.permission.READ_PHONE_STATE);
            permissionsNeeded.add(Manifest.permission.RECEIVE_SMS);
            permissionsNeeded.add(Manifest.permission.READ_EXTERNAL_STORAGE);
            permissionsNeeded.add(Manifest.permission.WRITE_EXTERNAL_STORAGE);
            permissionsNeeded.add(Manifest.permission.CAMERA);
            List<String> permissionsNeededActual = new ArrayList<>();
            for (int i = 0; i < permissionsNeeded.size(); i++) {
                if (ContextCompat.checkSelfPermission(activity, permissionsNeeded.get(i)) != PackageManager.PERMISSION_GRANTED) {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(activity, permissionsNeeded.get(i))) {

                        break;
                    }
                    permissionsNeededActual.add(permissionsNeeded.get(i));
                }
            }
            if (permissionsNeededActual.size() > 0){
                activity.requestPermissions(permissionsNeededActual.toArray(new String[permissionsNeededActual.size()]), 1);
            }

        }

    }
}
