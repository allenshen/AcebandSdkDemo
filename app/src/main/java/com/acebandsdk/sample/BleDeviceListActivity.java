package com.acebandsdk.sample;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import com.xiaoqu.aceband.ble.bean.BandDeviceBean;
import com.xiaoqu.aceband.sdk.AceBandSDKManager;
import com.xiaoqu.aceband.sdk.ScanDeviceListener;
import java.util.ArrayList;
import java.util.List;

import butterknife.Bind;
import butterknife.ButterKnife;

public class BleDeviceListActivity extends Activity implements OnItemClickListener {
    public static final int RESULT_CODE = 31;
    private static final int REQUEST_ENABLE_BT = 1;
    public final static String EXTRA_DEVICE_ADDRESS = "EXTRA_DEVICE_ADDRESS";
    public final static String EXTRA_DEVICE_NAME = "EXTRA_DEVICE_NAME";
    public final static String FILTERNAME = null;
    @Bind(R.id.listView)
    ListView listView;
    private BleDeviceListAdapter adapter;
    String filterName = "aceband";
    private AceBandSDKManager sdkManager;
    private long lastFindDeviceTime;
    private List<BandDeviceBean> tempBandDeviceList = new ArrayList<>();
    private ScanDeviceListener scanDeviceListener = new ScanDeviceListener() {

        @Override
        public void scanFinish() {
            if(tempBandDeviceList.size()>0){
                adapter.addDeviceItems(tempBandDeviceList);
                tempBandDeviceList.clear();
            }

        }

        @Override
        public void scanFindOneDevice(BandDeviceBean bandDeviceBean) {
            if (adapter.macaddressMap.containsKey(bandDeviceBean.getAddress()))return;
            if(System.currentTimeMillis()-lastFindDeviceTime>=100) {//避免刷新过快导致UI ANR
                adapter.addDeviceItem(bandDeviceBean);
                if(tempBandDeviceList.size()>0){
                    adapter.addDeviceItems(tempBandDeviceList);
                    tempBandDeviceList.clear();
                }
            }else{
                tempBandDeviceList.add(bandDeviceBean);
            }

            lastFindDeviceTime = System.currentTimeMillis();
        }
    };



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.device_list);
        ButterKnife.bind(this);
        listView = (ListView) findViewById(R.id.listView);
        adapter = new BleDeviceListAdapter(this);
        listView.setAdapter(adapter);
        listView.setOnItemClickListener(this);
        sdkManager = AceBandSDKManager.getInstance();
        GetPermissionUtil.getPermissions(this);
        if(PrefenceUtil.readBooleanPreference(PrefenceUtil.PRE_HAS_BIND_DEVICE,false)){
            Intent intent = new Intent(this,MainActivity.class);
            startActivity(intent);
            finish();
            return;
        }



    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main, menu);
        if (sdkManager.isScanning()==false) {
            menu.findItem(R.id.menu_stop).setVisible(false);
            menu.findItem(R.id.menu_scan).setVisible(true);
            menu.findItem(R.id.menu_refresh).setActionView(null);
        } else {
            menu.findItem(R.id.menu_stop).setVisible(true);
            menu.findItem(R.id.menu_scan).setVisible(false);
            menu.findItem(R.id.menu_refresh).setActionView(
                    R.layout.actionbar_indeterminate_progress);

        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (sdkManager.isScanning() == false) {
            adapter.clearAllDevieceItem();
            sdkManager.startScanDevice(scanDeviceListener);
        } else {
            sdkManager.stopScanDevice();
        }
        invalidateOptionsMenu();
        return true;
    }

    @Override
    protected void onResume() {
        super.onResume();
        adapter.clearAllDevieceItem();
        if (sdkManager.isScanning() == false) {
            adapter.clearAllDevieceItem();
            sdkManager.startScanDevice(scanDeviceListener);
        } else {
            sdkManager.stopScanDevice();
        }
        invalidateOptionsMenu();

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        // User chose not to enable Bluetooth.
        if (requestCode == REQUEST_ENABLE_BT && resultCode == Activity.RESULT_CANCELED) {
            finish();
            return;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    protected void onPause() {
        super.onPause();
        sdkManager.stopScanDevice();
        invalidateOptionsMenu();


    }


    @Override
    protected void onDestroy() {
        super.onDestroy();
    }


    public void onItemClick(AdapterView<?> adapterView, View view,
                            int position, long id) {
        sdkManager.stopScanDevice();
        BandDeviceBean device = (BandDeviceBean) adapter.listItems
                .get(position);
        Intent intent = new Intent(BleDeviceListActivity.this, MainActivity.class);
        intent.putExtra("BluetoothDevice",device.getDevice());
        startActivity(intent);
        finish();


    }


}
