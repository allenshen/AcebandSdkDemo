package com.acebandsdk.sample;

import android.service.notification.StatusBarNotification;

import com.xiaoqu.aceband.ble.util.Debug;
import com.xiaoqu.aceband.sdk.AceBandSDKManager;
import com.xiaoqu.aceband.sdk.BleTaskListener;
import com.xiaoqu.aceband.sdk.MessageRemindType;
import com.xiaoqu.aceband.sdk.notification.NotificationListener;

/**
 * Created by chenshen on 2016/1/26.
 */
public class MessageService extends NotificationListener {
    private String TAG = "MsgService";
    private BleTaskListener listener = new BleTaskListener() {
        @Override
        public void onSuccess(String jsonData) {

        }

        @Override
        public void onFailed(String jsonData) {

        }
    };

    @Override
    public void onNotificationPosted(StatusBarNotification sbn) {
        super.onNotificationPosted(sbn);
        Debug.logV(TAG, "packageName " + packageName);
        Debug.logV(TAG, "messageTitle " + messageTitle);
        Debug.logV(TAG, "messageContent " + messageContent);
        AceBandSDKManager sdkManager = AceBandSDKManager.getInstance();
        if (PACKAGE_QQ.equals(packageName)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.QQ_TASNS_TYPE,messageTitle,messageContent);
        } else if (PACKAGE_WX.equals(packageName)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.WECHAT_TASNS_TYPE,messageTitle,messageContent);
        } else if (packageName.equals(WHATSAPP_PACKAGENAME)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.WHATSAPP_TASNS_TYPE,messageTitle,messageContent);
        } else if (packageName.equals(GMAILAPP_PACKAGEBAME) || packageName.equals(GMAILAPP_PACKAGEBAME_TWO) || packageName.equals(WANGYIEMAIL_PACKAGENAME)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.WECHAT_TASNS_TYPE,messageTitle,messageContent);
        } else if (packageName.equals(TWITTER_PACKAGENAME)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.TWITTER_TASNS_TYPE,messageTitle,messageContent);
        } else if (packageName.equals(FACEBOOK_PACKAGENAME)||FACEBOOK_PACKAGENAME2.equals(packageName)) {
            sdkManager.sendTextRemind(listener, MessageRemindType.FACEBOOK_TASNS_TYPE,messageTitle,messageContent);
        }
    }
    


}
